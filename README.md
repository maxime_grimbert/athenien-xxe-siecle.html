# Un athénien au XXe siècle

Mise en forme modernisée d'un site web existant mais au design obsolète. La totalité du contenu (texte et images) provient du site existant.

Mise en forme réalisée en 2015 (initialement hors de git).

## Reste à faire : 

- Mentions légales (avec code d'intégration automatique)
- Plan du site (avec code d'intégration automatique)

- Peinture : p. 3 à 6 (dont les retouches d'images), mais les modèles existent

- Poésie : p1 à terminer (et images à retoucher), p. 2 à 9 à faire (dont les images), mais les modèles existent

- Vie : p1 à terminer (et les images à retoucher), p. 2 à 8 à faire (dont les images) mais les modèles existent
